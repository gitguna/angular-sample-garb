import {Component, Input} from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'usercard',
  templateUrl: 'usercard.component.html',
  styleUrls: ['usercard.component.css'],
})
export class userCard {
    @Input()
    person : any = {};
    constructor(private http:HttpClient){

    }
    delete(){
        (this.http.get("http://localhost:8080/deletePerson?id="+this.person.id)).subscribe((data:any)=>{ window.location.reload();});
    }

}