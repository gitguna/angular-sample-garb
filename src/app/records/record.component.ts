import { HttpClient } from "@angular/common/http";
import { Component,OnInit } from "@angular/core";
import { Observable } from "rxjs";

interface Record {
    id: Number,
    name: String
}

@Component({
    selector:"app-records",
    templateUrl:"./record.component.html",
    styleUrls:['./record.component.css']
})
export class RecordsComponent{
    // Call to backend to 
    
    readonly URL="http://localhost:8080/listPersons";
    persons!: any[];

    readonly API_EP="https://randomuser.me/api/?inc=title,name,email,picture,gender,login&noinfo";
    readonly BACKEND_SEND="http://localhost:8080/addPerson"
    readonly BACKIMG_URL="https://random.imagecdn.app/500/500"
    userFull:any
    personObj:any

    constructor(private http:HttpClient){
        console.log("Constructor");
        (this.http.get(this.URL)).subscribe((data:any)=>{
            this.persons=data;
            // console.log(data)
        });
    }

    
    addRecords(){
        this.userFull=(this.http.get(this.API_EP)).subscribe((userFull:any)=>{
            console.log(userFull)
        this.personObj={
            id:userFull.results[0].login.uuid,
            title:userFull.results[0].name.title,
            first:userFull.results[0].name.first,
            last:userFull.results[0].name.last,
            email:userFull.results[0].email,
            photoThumb:userFull.results[0].picture.thumbnail,
            photoLarge:userFull.results[0].picture.large,
            gender:userFull.results[0].gender

        };
        console.log(this.personObj);
        (this.http.post(this.BACKEND_SEND,this.personObj)).subscribe((data:any)=>{ window.location.reload();});
        
        });
        
        
    }

}
   